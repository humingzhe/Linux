##  Resource

访问google
    
    http://coderschool.cn/1853.html
    https://google.jiongjun.cc/
    https://vvpn.499994.xyz/googlebak.html

CentOS 7.4 下载地址
    
    64位  http://mirrors.sohu.com/centos/7.4.1708/isos/x86_64/CentOS-7-x86_64-DVD-1708.iso
    
    32位  http://mirror.centos.org/altarch/7.4.1708/isos/i386/CentOS-7-i386-DVD-1708.iso （如果你能安装64位，请不要下载此版本，很多问题，安装此镜像，需要创建64位的centos虚拟机）

CentOS 7 下载地址

    https://mirrors.tuna.tsinghua.edu.cn/centos/7/isos/x86_64/CentOS-7-x86_64-Everything-1708.iso

CentOS 6.9 下载地址
    
    64位  https://mirrors.tuna.tsinghua.edu.cn/centos/6.9/isos/x86_64/CentOS-6.9-x86_64-bin-DVD1.iso
          
          https://mirrors.tuna.tsinghua.edu.cn/centos/6.9/isos/x86_64/CentOS-6.9-x86_64-bin-DVD2.iso
    
    32位  https://mirrors.tuna.tsinghua.edu.cn/centos/6.9/isos/i386/CentOS-6.9-i386-bin-DVD1.iso
          
          https://mirrors.tuna.tsinghua.edu.cn/centos/6.9/isos/i386/CentOS-6.9-i386-bin-DVD2.iso

MySQL下载地址：

    5.1 32位二进制包： http://mirrors.sohu.com/mysql/MySQL-5.1/mysql-5.1.73-linux-i686-glibc23.tar.gz
   
    5.1_64位二进制包：http://mirrors.sohu.com/mysql/MySQL-5.1/mysql-5.1.73-linux-x86_64-glibc23.tar.gz
    
    5.5_64位二进制包：http://mirrors.sohu.com/mysql/MySQL-5.5/mysql-5.5.55-linux2.6-x86_64.tar.gz    
   
    5.5_32位二进制包：http://mirrors.sohu.com/mysql/MySQL-5.5/mysql-5.5.55-linux2.6-i686.tar.gz
   
    5.6_32位二进制包：http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.36-linux-glibc2.5-i686.tar.gz
   
    5.6_64位二进制包：http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.36-linux-glibc2.5-x86_64.tar.gz
    
    5.5源码包：http://mirrors.sohu.com/mysql/MySQL-5.5/mysql-5.5.55.tar.gz
    
    5.6源码包：http://mirrors.sohu.com/mysql/MySQL-5.6/mysql-5.6.36.tar.gz

MariaDB下载地址：
    
    10.2.6 64位二进制包 https://downloads.mariadb.com/MariaDB/mariadb-10.2.6/bintar-linux-glibc_214-x86_64/mariadb-10.2.6-linux-glibc_214-x86_64.tar.gz
            
    10.2.6 32位二进制包 https://downloads.mariadb.com/MariaDB/mariadb-10.2.6/bintar-linux-glibc_214-x86/mariadb-10.2.6-linux-glibc_214-i686.tar.gz

Apache下载地址：
    
    2.2源码包下载地址  http://mirrors.sohu.com/apache/httpd-2.2.34.tar.gz
    
    2.4源码包下载地址  http://mirrors.sohu.com/apache/httpd-2.4.29.tar.gz
    
    Apr下载地址        http://mirrors.cnnic.cn/apache/apr/apr-1.6.3.tar.gz
    
    Apr-util下载地址   apr-util: http://mirrors.cnnic.cn/apache/apr/apr-util-1.6.1.tar.bz2

Nginx下载地址：

    1.8源码包： http://nginx.org/download/nginx-1.8.0.tar.gz

    1.6源码包： http://nginx.org/download/nginx-1.6.3.tar.gz

    1.4源码包： http://nginx.org/download/nginx-1.4.7.tar.gz

PHP下载地址：

    5.6源码包： http://cn2.php.net/distributions/php-5.6.30.tar.bz2

    5.5源码包： http://cn2.php.net/distributions/php-5.5.38.tar.bz2

    5.4源码包： http://cn2.php.net/distributions/php-5.4.45.tar.bz2
                
    7.1源码包： http://cn2.php.net/distributions/php-7.1.6.tar.bz2
                        
    其他版本： http://php.net/releases/

memcache php扩展下载地址：

    http://www.apelearn.com/bbs/data/attachment/forum/memcache-2.2.3.tgz
        
memcached下载地址：

     http://memcached.org/files/memcached-1.4.30.tar.gz
                
redis下载地址
    
    https://codeload.github.com/antirez/redis/tar.gz/2.8.21  (下载后改名redis-2.8.21.tar.gz)
                        
redis php扩展下载地址：

    https://codeload.github.com/phpredis/phpredis/zip/develop （下载后改名phpredis-develop.zip）
                                
mongodb php扩展下载地址：
    
    https://github.com/mongodb/mongo-php-driver-legacy/archive/master.zip
                                        
pureftp下载地址：
    
    https://download.pureftpd.org/pub/pure-ftpd/releases/pure-ftpd-1.0.45.tar.bz2

    https://download.pureftpd.org/pub/pure-ftpd/releases/pure-ftpd-1.0.45.tar.gz
    
java JDK下载地址：

    1.9版本 http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html    

    1.8版本 http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

    1.7版本下载地址  http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html

tomcat下载地址：

    源码包：https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.24/src/apache-tomcat-8.5.24-src.tar.gz

    二进制软件包：https://mirrors.tuna.tsinghua.edu.cn/apache/tomcat/tomcat-8/v8.5.24/bin/apache-tomcat-8.5.24.tar.gz

    7版本二进制：http://mirrors.hust.edu.cn/apache/tomcat/tomcat-7/v7.0.82/bin/apache-tomcat-7.0.82.tar.gz
    
    7版本源码包：http://mirrors.hust.edu.cn/apache/tomcat/tomcat-7/v7.0.82/src/apache-tomcat-7.0.82-src.tar.gz

tomcat连接mysql驱动JDBC下载地址：
    
    官方地址：https://dev.mysql.com/downloads/file/?id=468318
            
    下载地址：https://cdn.mysql.com//Downloads/Connector-J/mysql-connector-java-5.1.41.tar.gz

resin下载地址：

    官  方：http://caucho.com/download/resin-4.0.45.tar.gz （很慢）

    胡明哲：https://gitlab.com/humingzhe/Linux/blob/master/Resource/redis-4.0.8.tar.gz （较快）

zabbix下载
  
      官方：https://www.zabbix.com/download

    
